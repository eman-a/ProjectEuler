def isPrime (arg):
  if arg > 2 and arg % 2 != 0:
    n = arg // 2

    for i in range(2, n + 1):
       if arg % i == 0:
           return False
    return True
  elif arg > 2:
    return False
  else:
    return True
count = 0
number = raw_input("Enter the number that you want the sum of its primes: ")
try:
  number = int(number)
except:
  print 'Enter a valid integer!'
for i in range(2, number):
    if isPrime(i):
#      print i
       count = count + i
    else:
       continue
print count

